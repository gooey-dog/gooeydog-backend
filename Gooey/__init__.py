from flask import Flask
from flask_restx import Api
from werkzeug.middleware.proxy_fix import ProxyFix

from .user import api as user_api
from .user.models import db as user_db
from .user.user import login_manager

from .commissions import api as commissions_api
from .commissions.models import db as commissions_db

from .artwork import api as artwork_api
from .artwork.models import db as artwork_db



# Flask
app = Flask(__name__)
app.config.from_object("config.DevelopmentConfig")
app.wsgi_app = ProxyFix(app.wsgi_app)

# RestX
api = Api(title="Gooey.Dog API", version="0.1", description="A Flask back-end for Gooey.Dog")
api.add_namespace(user_api)
api.add_namespace(commissions_api)
api.add_namespace(artwork_api)

# Installing
user_db.init_app(app)
commissions_db.init_app(app)
artwork_db.init_app(app)
api.init_app(app)
login_manager.init_app(app)

# Init tables (don't do this)
# with app.app_context():
#     db.create_all()
