from flask_restx import Model, fields

artwork_model = Model(
    "Artwork",
    {
        "id": fields.Integer(required=True),
        "title": fields.String(required=True),
        "description": fields.String(required=True),
        "url": fields.String(required=True),
        "links": fields.List(fields.String())
    }
)
