from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship

db = SQLAlchemy()


# A one-to-many relationship
class Artwork(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(1024), nullable=False)
    url = db.Column(db.String(512), nullable=False)

    links = relationship("ArtworkLink", cascade="all, delete")


class ArtworkLink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(512), nullable=False)

    artwork_id = db.Column(db.Integer, db.ForeignKey('artwork.id'), nullable=False)
