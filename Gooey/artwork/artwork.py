from flask_login import current_user
from flask_restx import Namespace, Resource, fields

from .models import Artwork, ArtworkLink, db
from .marshalling import artwork_model

api = Namespace("artwork", description="My artworks")
api.models[artwork_model.name] = artwork_model


# Helpers
def format_artwork(artwork):
    links = [link.link for link in artwork.links]
    return {
        "id": artwork.id,
        "title": artwork.title,
        "description": artwork.description,
        "url": artwork.url,
        "links": links
    }


@api.route('/')
class ArtworkListResource(Resource):
    @api.doc('artwork-list')
    @api.marshal_list_with(artwork_model)
    def get(self):
        """Get a list of all artworks"""
        def format_artwork(artwork):
            links = [link.link for link in artwork.links]
            return {
                "id": artwork.id,
                "title": artwork.title,
                "description": artwork.description,
                "url": artwork.url,
                "links": links
            }

        return [format_artwork(artwork) for artwork in Artwork.query.all()]

    @api.doc('artwork-post')
    @api.expect(artwork_model, validate=True)
    @api.response(201, 'Artwork created.')
    def post(self):
        """Post a new artwork. Requires admin rights."""
        if not current_user.is_authenticated or not current_user.admin:
            api.abort(401, "Unauthorized. Must be logged in as admin.")

        new_artwork = Artwork(
            title=api.payload['title'],
            description=api.payload['description'],
            url=api.payload['url']
        )
        db.session.add(new_artwork)
        db.session.commit()

        for link in api.payload['links']:
            new_link = ArtworkLink(
                link=link,
                artwork_id=new_artwork.id
            )
            db.session.add(new_link)
        db.session.commit()
        return 'Artwork created', 201


@api.route('/<int:id>')
@api.param('id', 'Artwork id')
class ArtworkResource(Resource):
    @api.doc('artwork-get')
    @api.response(404, 'Artwork not found.')
    @api.marshal_with(artwork_model)
    def get(self, id):
        """Get a single artwork based on its id"""
        res = Artwork.query.get(id)
        if res:
            return res
        else:
            api.abort(404, "Artwork not found.")

    @api.doc('artwork-delete')
    @api.response(204, 'Successfully deleted.')
    def delete(self, id):
        """Delete a single artwork based on its id. Requires admin rights"""
        if not current_user.is_authenticated or not current_user.admin:
            api.abort(401, "Unauthorized. Must be logged in as admin.")

        res = Artwork.query.get(id)
        if not res:
            api.abort(404, "Artwork not found.")

        db.session.delete(res)
        db.session.commit()
        return "Successfully deleted.", 204

    @api.doc('artwork-put')
    @api.response(200, 'Artwork updated.')
    @api.expect(artwork_model, validate=True)
    @api.marshal_with(artwork_model)
    def patch(self, id):
        """Update/replace a single artwork based on its id. Requires admin rights"""
        if not current_user.is_authenticated or not current_user.admin:
            api.abort(401, "Unauthorized. Must be logged in as admin.")

        res = Artwork.query.get(id)
        if not res:
            api.abort(404, "Artwork not found.")

        # Update info
        res.title = api.payload['title']
        res.description = api.payload['description']
        res.url = api.payload['url']
        # Delete all previous links
        for previous_link in ArtworkLink.query.filter_by(artwork_id=res.id).all():
            db.session.delete(previous_link)
        # Add new links
        for link in api.payload['links']:
            new_link = ArtworkLink(
                link=link,
                artwork_id=res.id
            )
            db.session.add(new_link)
        db.session.commit()
        return format_artwork(res)
