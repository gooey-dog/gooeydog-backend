import psycopg2.errors
from sqlalchemy.exc import DataError
from flask_login import current_user
from flask_restx import Namespace, Resource

from .models import db, QueueItem
from .marshalling import commission_type_model, \
    commission_style_model, \
    commission_extras_model, \
    commission_background_model, \
    commission_price_model, \
    commission_background_price_model, \
    commission_addon_model, \
    commission_info_model, \
    queue_item_model

# Create namespace and register the models
api = Namespace("commissions", description="Information about commissions: prices, and queue")
api.models[commission_type_model.name] = commission_type_model
api.models[commission_style_model.name] = commission_style_model
api.models[commission_extras_model.name] = commission_extras_model
api.models[commission_background_model.name] = commission_background_model
api.models[commission_price_model.name] = commission_price_model
api.models[commission_background_price_model.name] = commission_background_price_model
api.models[commission_addon_model.name] = commission_addon_model
api.models[commission_info_model.name] = commission_info_model

api.models[queue_item_model.name] = queue_item_model


@api.route('/info')
class InfoResource(Resource):
    @api.doc('info-get')
    @api.marshal_with(commission_info_model)
    def get(self):
        """Get all information about commissions."""
        # TODO
        pass


@api.route('/queue')
class QueueListResource(Resource):
    @api.doc('queue-get')
    @api.marshal_list_with(queue_item_model)
    def get(self):
        """Get all queue items."""
        return QueueItem.query.all()

    @api.doc('queue-post')
    @api.expect(queue_item_model, validate=True)
    @api.response(201, 'Queue item created')
    @api.marshal_with(queue_item_model)
    def post(self):
        """Add a new queue item. Requires admin rights."""
        if not current_user.is_authenticated or not current_user.admin:
            api.abort(401, "Unauthorized. Must be logged in as admin.")

        new_queue_item = QueueItem(
            owner=api.payload['owner'],
            status=api.payload['status'],
            description=api.payload['description'],
            preview_url=api.payload['preview_url'],
            updated=api.payload['updated']
        )

        db.session.add(new_queue_item)
        try:
            db.session.commit()
            return new_queue_item
        except DataError:
            api.abort(400, "Bad status. Enums are: QUEUED, CONFIRMED, STARTED, FINISHED")



@api.route('/queue/<int:id>')
@api.response(404, 'Queue item not found.')
@api.param('id', 'Queue item id')
class QueueItemResource(Resource):
    @api.doc('queue-item-get')
    @api.marshal_with(queue_item_model)
    def get(self, id):
        """Get a queue item by id."""
        res = QueueItem.query.get(id)
        if not res:
            api.abort(404, "Queue item not found.")
        return res

    @api.response(204, 'Queue item successfully deleted')
    @api.doc('queue-item-delete')
    def delete(self, id):
        """Delete a queue item by id"""
        res = QueueItem.query.get(id)
        if not res:
            api.abort(404, "Queue item not found.")
        db.session.delete(res)
        db.session.commit()
        return "Queue item successfully deleted", 204

    @api.response(200, 'Queue item successfully updated')
    @api.doc('queue-item-update')
    @api.expect(queue_item_model, validate=True)
    @api.marshal_with(queue_item_model)
    def patch(self, id):
        """Update a queue item by id"""
        res = QueueItem.query.get(id)
        if not res:
            api.abort(404, "Queue item not found.")

        res.owner = api.payload['owner'],
        res.status = api.payload['status'],
        res.description = api.payload['description'],
        res.preview_url = api.payload['preview_url'],
        res.updated = api.payload['updated']
        return res
