import datetime
import enum

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# Commission Info
class CommissionStyle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    previewUrl = db.Column(db.String(512), nullable=True)


class CommissionType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)


class CommissionBackground(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    previewUrl = db.Column(db.String(512), nullable=True)


class CommissionExtra(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    price = db.Column(db.Float, nullable=True)

    typeId = db.Column(db.Integer, db.ForeignKey('commission_type.id'), nullable=False)


# Commission Queue
class Status(enum.Enum):
    QUEUED = 1
    CONFIRMED = 2
    STARTED = 3
    FINISHED = 4


class QueueItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.String(256), nullable=True)
    status = db.Column(db.Enum(Status), nullable=False, default='QUEUED')
    description = db.Column(db.String(512), nullable=True)
    preview_url = db.Column(db.String(512), nullable=True)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow())
