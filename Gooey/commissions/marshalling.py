from flask_restx import Model, fields

# Commission Info
commission_style_model = Model(
    "CommissionStyle",
    {
        "id": fields.Integer(required=True),
        "name": fields.String(required=True),
        "previewUrl": fields.String(required=True),
    }
)

commission_type_model = Model(
    "CommissionType",
    {
        "id": fields.Integer(required=True),
        "name": fields.String(required=True),
    }
)

commission_background_model = Model(
    "CommissionBackground",
    {
        "id": fields.Integer(required=True),
        "name": fields.String(required=True),
        "previewUrl": fields.String(required=True),
    }
)

commission_extras_model = Model(
    "CommissionExtras",
    {
        "id": fields.Integer(required=True),
        "name": fields.String(required=True),
        "typeId": fields.Integer(required=True),
        "price": fields.Float(required=True),
    }
)

commission_price_model = Model(
    "CommissionPrice",
    {
        "typeId": fields.Integer(required=True),
        "styleId": fields.Integer(required=True),
        "price": fields.Float(required=True),
    }
)

commission_background_price_model = Model(
    "CommissionBackgroundPrice",
    {
        "typeId": fields.Integer(required=True),
        "backgroundId": fields.Integer(required=True),
        "price": fields.Float(required=True),
    }
)

commission_addon_model = Model(
    "CommissionAddons",
    {
        "id": fields.Integer(required=True),
        "name": fields.String(required=True),
        "priceMultiplier": fields.Float(required=True),
    }
)

commission_info_model = Model(
    "CommissionInfo",
    {
        "styles": fields.List(fields.Nested(commission_style_model)),
        "types": fields.List(fields.Nested(commission_type_model)),
        "extras": fields.List(fields.Nested(commission_extras_model)),
        "backgrounds": fields.List(fields.Nested(commission_background_model)),
        "prices": fields.List(fields.Nested(commission_price_model)),
        "backgroundPrices": fields.List(fields.Nested(commission_background_price_model)),
        "addons": fields.List(fields.Nested(commission_addon_model)),
    }
)

# Queue Items
queue_item_model = Model(
    "QueueItem",
    {
        "id": fields.Integer(required=True),
        "owner": fields.String(required=False),
        "status": fields.String(required=True),
        "description": fields.String(required=True),
        "preview_url": fields.String(required=False),
        "updated": fields.DateTime(required=True)
    }
)
