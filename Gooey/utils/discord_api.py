from flask import current_app
from requests_oauthlib import OAuth2Session


def make_oauth_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=current_app.config['DISCORD_CLIENT_ID'],
        token=token,
        state=state,
        scope=scope,
        redirect_uri=current_app.config['DISCORD_REDIRECT_URI'],
    )


def authorization_url(scope='identify'):
    session = make_oauth_session(scope=scope)
    return session.authorization_url(current_app.config['DISCORD_AUTHORIZATION_BASE_URL'])


def fetch_token(state, authorization_response):
    session = make_oauth_session(state=state)
    return session.fetch_token(
        current_app.config['DISCORD_TOKEN_URL'],
        client_secret=current_app.config['DISCORD_CLIENT_SECRET'],
        authorization_response=authorization_response)


def revoke_token(token):
    session = make_oauth_session(token=token)
    return session.get(current_app.config['DISCORD_TOKEN_REVOCATION_URL'])


def me(token):
    session = make_oauth_session(token=token)
    return session.get(current_app.config['DISCORD_TOKEN_USER_URL']).json()
