from http import HTTPStatus

from flask import session, request, redirect
from flask_restx import Namespace, Resource
from flask_login import LoginManager, login_user, current_user, logout_user

import Gooey.utils.discord_api as discord

from .models import User, user_model, db, DiscordUser

login_manager = LoginManager()

# Start api and register Marshalling models
api = Namespace("user", description="User authentication with Discord OAuth2")
api.models[user_model.name] = user_model


# Helpers
def format_user(user: User):
    return {
        "discord_id": user.discord_user.discord_id,
        "admin": user.admin,
        "username": user.discord_user.username,
        "discriminator": user.discord_user.discriminator,
        "avatar": user.discord_user.avatar
    }


# Resources
@login_manager.user_loader
def load_user(user_id):
    # All ways of doing it
    # Old
    # res = db.session.query(User).filter(User.id == int(user_id)).first()
    # New (1.4/2.0)
    # res = db.session.execute(select(User).where(User.id == int(user_id))).scalar_one()
    # Flask-SQLAlchemy
    res = User.query.get(int(user_id))
    # print(f'In load_user: res:{res}')
    return res


@api.route("/login")
class UserLoginResource(Resource):
    @api.doc('login')
    @api.response(303, "Redirect to Discord OAuth2 server")
    def get(self):
        """Start login procedures and redirect to OAuth2 server"""
        redirect_url, state = discord.authorization_url()
        session['state'] = state
        return redirect(redirect_url)


@api.route("/logout")
class UserLogoutResource(Resource):
    @api.doc('logout')
    @api.response(HTTPStatus.OK, "Logged out successfully")
    def get(self):
        """Log out"""
        logout_user()
        return "Logged out successfully", HTTPStatus.OK


# TODO: Refactor big class
@api.route("/callback")
class UserLoginCallbackResource(Resource):
    @api.doc('login-callback')
    @api.marshal_with(user_model)
    def get(self):
        """Finish the login procedures after OAuth2 authentication"""
        if request.values.get('error'):
            api.abort(HTTPStatus.UNAUTHORIZED, request.values['error'])
        user_token = discord.fetch_token(
            state=session['state'],
            authorization_response=request.url)
        discord_user_response = discord.me(user_token)
        discord.revoke_token(user_token)

        discord_user = DiscordUser(
            discord_id=discord_user_response['id'],
            username=discord_user_response['username'],
            discriminator=discord_user_response['discriminator'],
            avatar=discord_user_response['avatar']
        )

        stored_discord_user = DiscordUser.query.filter_by(discord_id=discord_user.discord_id).first()

        # If this discord user doesn't exist, let's make a new account for them, else update the DiscordUser.
        if not stored_discord_user:
            db.session.add(discord_user)
            db.session.commit()
            # discord_user should now have a valid id
            db.session.add(User(discord_user_id=discord_user.id))
            db.session.commit()
            stored_discord_user = discord_user
        else:
            stored_discord_user.username = discord_user.username
            stored_discord_user.discriminator = discord_user.discriminator
            stored_discord_user.avatar = discord_user.avatar
            db.session.commit()

        login_user(stored_discord_user.app_user, remember=True)

        return format_user(stored_discord_user.app_user)


@api.route("/")
class UserResource(Resource):
    @api.doc('get_logged_in_user')
    @api.marshal_with(user_model)
    def get(self):
        """Get currently logged in user"""
        if current_user.is_authenticated:
            stored_user = User.query.get(current_user.id)
            if stored_user:
                return format_user(stored_user)
        api.abort(401, "Not logged in")
