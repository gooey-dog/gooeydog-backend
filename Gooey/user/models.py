from datetime import datetime

from flask_login import UserMixin
from flask_restx import fields, Model
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship

db = SQLAlchemy()

# Marshalling
user_model = Model(
    "User",
    {
        "discord_id": fields.String(required=True),
        "admin": fields.Boolean(required=True),
        "username": fields.String(required=True),
        "discriminator": fields.String(required=True),
        "avatar": fields.String(),
    }
)


# Business Objects
class DiscordUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    discord_id = db.Column(db.String(128), nullable=False, unique=True)
    username = db.Column(db.String(32), nullable=False)
    discriminator = db.Column(db.String(4), nullable=False)
    avatar = db.Column(db.String(256), nullable=True)

    app_user = relationship("User", back_populates="discord_user", uselist=False)

    def __repr__(self):
        return f'DiscordUser({self.id}:{self.username}#{self.discriminator})'


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    admin = db.Column(db.Boolean, default=False)
    signup_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())

    discord_user_id = db.Column(db.Integer, db.ForeignKey('discord_user.id'), unique=True)
    discord_user = relationship("DiscordUser", back_populates="app_user", uselist=False)

    # For flask-login
    is_active = True
    is_anonymous = False

    # For flask-login: gets the id as a unicode string
    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return f'User({self.id}, admin={str(self.admin)})'
