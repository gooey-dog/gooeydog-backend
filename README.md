# GooeyDog Backend

REST Back-end for the Gooey.Dog website!

Uses Flask with Flask-RestX as its framework.

Uses SQLAlchemy as an ORM.
